#define BLYNK_PRINT Serial

int pin = 14; // pino onde o LED está conectado

#include <WiFi.h>
#include <WiFiClient.h>
#include <BlynkSimpleEsp32.h>

char auth[] = "SUA_CHAVE_DE_AUTENTICACAO";
char ssid[] = "SUA_REDE";
char pass[] = "SUA_SENHA";

void setup() {
	pinMode(pin, OUTPUT);

	Serial.begin(115200);

	delay(10);
	Serial.print("Connecting to ");
	Serial.println(ssid);

	WiFi.begin(ssid, pass);

	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");
	}

	Serial.println("WiFi connected");

	Blynk.begin(auth, ssid, pass);
}

void loop(){
	Blynk.run();
}
