# Explicações

- `Blynk-exemplo.c`: pisca LED por aplicativo de celular
- `esp32blink.c`: pisca LED (sem WiFi), para testar funcionamento
- `WebServer-simplificado.c`: pisca LED por WiFi, utilizando somente o LED da
  própria placa
- `WebServer.c`: pisca LED por WiFi, utilizando LEDs externos
- `WebServer-ip-fixo.c`: exemplo utilizando IP fixo na ESP32 (sem DNS)
- `WifiScan.c`: exemplo que escaneia Wifis rodeados pela ESP32
