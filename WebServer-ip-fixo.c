#include <WiFi.h>

const char* ssid = "NOME-DA-REDE"; // nome do WiFi
const char* password = "SENHA-DA-REDE"; // senha

WiFiServer server(7777); // porta ao qual o server será exposto

String header;

String output26State = "off";
String output27State = "off";
const int output26 = 26;
const int output27 = 27;

IPAddress local_IP(192, 168, 1, 31); // IP da rede interna
IPAddress gateway(192, 168, 1, 1); // Gateway da rede interna

IPAddress subnet(255, 255, 255, 0);
IPAddress primaryDNS(8, 8, 8, 8); // DNS primário
IPAddress secondaryDNS(4, 4, 4, 4); // DNS secundário


void setup() {
	Serial.begin(115200);
	pinMode(output26, OUTPUT);
	pinMode(output27, OUTPUT);
	digitalWrite(output26, LOW);
	digitalWrite(output27, LOW);

	if (!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS)) {
		Serial.println("STA falhou");
	}

	Serial.print("Conectando na rede ");
	Serial.println(ssid);
	WiFi.begin(ssid, password);
	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");
	}
	Serial.println("");
	Serial.println("WiFi conectada.");
	Serial.println("Endereco IP: ");
	Serial.println(WiFi.localIP());
	Serial.print("Subnet Mask: ");
	Serial.println(WiFi.subnetMask());
	Serial.print("Gateway IP: ");
	Serial.println(WiFi.gatewayIP());
	Serial.print("DNS 1: ");
	Serial.println(WiFi.dnsIP(0));
	Serial.print("DNS 2: ");
	Serial.println(WiFi.dnsIP(1));

	server.begin();
}

void loop(){
	WiFiClient client = server.available(); // Listen for incoming clients

	if (client) { // If a new client connects,
		Serial.println("New Client."); // print a message out in the serial port
		String currentLine = ""; // make a String to hold incoming data from the client
		while (client.connected()) { // loop while the client's connected
			if (client.available()) { // if there's bytes to read from the client,
				char c = client.read(); // read a byte, then
				Serial.write(c); // print it out the serial monitor
				header += c;
				if (c == '\n') { // if the byte is a newline character
					// if the current line is blank, you got two newline characters in a row.
					// that's the end of the client HTTP request, so send a response:
					if (currentLine.length() == 0) {
						// HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
						// and a content-type so the client knows what's coming, then a blank line:
						client.println("HTTP/1.1 200 OK");
						client.println("Content-type:text/html");
						client.println("Connection: close");
						client.println();

						if (header.indexOf("GET /26/on") >= 0) {
							Serial.println("GPIO 26 on");
							output26State = "on";
							digitalWrite(output26, HIGH);
						} else if (header.indexOf("GET /26/off") >= 0) {
							Serial.println("GPIO 26 off");
							output26State = "off";
							digitalWrite(output26, LOW);
						} else if (header.indexOf("GET /27/on") >= 0) {
							Serial.println("GPIO 27 on");
							output27State = "on";
							digitalWrite(output27, HIGH);
						} else if (header.indexOf("GET /27/off") >= 0) {
							Serial.println("GPIO 27 off");
							output27State = "off";
							digitalWrite(output27, LOW);
						}

						client.println("<!DOCTYPE html><html>");
						client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
						client.println("<link rel=\"icon\" href=\"data:,\">");
						client.println("<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}");
						client.println(".button { background-color: #4CAF50; border: none; color: white; padding: 16px 40px;");
						client.println("text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}");
						client.println(".button2 {background-color: #555555;}</style></head>");
						client.println("<body><h1>ESP32 Web Server</h1>");

						client.println("<p>GPIO 26 - State " + output26State + "</p>");
						if (output26State=="off") {
							client.println("<p><a href=\"/26/on\"><button class=\"button\">ON</button></a></p>");
						} else {
							client.println("<p><a href=\"/26/off\"><button class=\"button button2\">OFF</button></a></p>");
						}

						client.println("<p>GPIO 27 - State " + output27State + "</p>");
						if (output27State=="off") {
							client.println("<p><a href=\"/27/on\"><button class=\"button\">ON</button></a></p>");
						} else {
							client.println("<p><a href=\"/27/off\"><button class=\"button button2\">OFF</button></a></p>");
						}
						client.println("</body></html>");
						client.println();
						break;
					} else { // se for nova linha, limpa buffer
						currentLine = "";
					}
				} else if (c != '\r') { // if you got anything else but a carriage return character,
					currentLine += c; // add it to the end of the currentLine
				}
			}
		}
		header = "";
		client.stop();
		Serial.println("Client disconnected.");
		Serial.println("");
	}
}
