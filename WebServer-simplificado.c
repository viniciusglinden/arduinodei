// Carrega biblioteca WiFi
#include <WiFi.h>

// Substitua com o nome e senha da conexão
const char* ssid = "NOME-DA-REDE"; // nome do WiFi
const char* password = "SENHA-DA-REDE"; // senha

// Cria objeto (C++) do servidor, provendo a porta de para acesso através do
// navegador
WiFiServer server(80);

// Variáveis para armazenar temporariamente os pacotes de clientes
String header;

// Variável auxiliar para armazenar o estado atual do LED
String LEDestado = "off";

// Pino do LED
const int LEDpino = 2;

void setup() {
	// Baud rate da porta serial
	Serial.begin(115200);
	// Configura o pino do LED como saída
	pinMode(LEDpino, OUTPUT);
	// Coloca valor baixo para o pino do LED
	digitalWrite(LEDpino, LOW);

	// Conecta com o WiFi, utilizando as informações acima
	Serial.print("Conectando ao ");
	Serial.println(ssid);
	WiFi.begin(ssid, password);
	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");
	}
	// Informa conexão via serial
	Serial.println("");
	Serial.println("WiFi conectado.");
	Serial.println("Endereço IP: ");
	Serial.println(WiFi.localIP());
	// Começa servidor
	server.begin();
}

void loop(){
	// Busca novo cliente
	WiFiClient client = server.available();

	// Se novo cliente conecta
	if (client) {
		// Indica novo cliente pela serial
		Serial.println("Novo Cliente.");
		// Cria uma variável para dados do cliente
		String currentLine = "";
		// Enquanto o cliente está conectado
		while (client.connected()) {
			// Se existe uma informação nova do cliente
			if (client.available()) {
				// Lê esta informação
				char c = client.read();
				// Escreve-a ao serial
				Serial.write(c);
				header += c;
				// Se a informação é uma nova linha
				if (c == '\n') {
					// Se a linha atual está vazia, tu tens duas novas linhas na
					// mesma informação. Isto significa o final do pedido HTTP,
					// então envie uma resposta:
					if (currentLine.length() == 0) {
						// Cabeçalho HTTP sempre começam com um código
						client.println("HTTP/1.1 200 OK");
						// E o tipo de conteúdo, para que o
						// Cliente saiba o que esperar
						client.println("Content-type:text/html");
						client.println("Connection: close");
						// E por fim uma linha em branco
						client.println();

						// Ligar/desligar o LED
						if (header.indexOf("GET /led/on") >= 0) {
							// Informa ao serial o estado do LED
							Serial.println("LED on");
							// Armazena informação na variável
							LEDestado = "on";
							// Liga o LED
							digitalWrite(LEDpino, HIGH);
						} else if (header.indexOf("GET /led/off") >= 0) {
							// Informa ao serial o estado do LED
							Serial.println("LED off");
							// Armazena informação na variável
							LEDestado = "off";
							// Desliga o LED
							digitalWrite(LEDpino, LOW);
						}

						// Envia o cabeçalho da página HTML
						client.println("<!DOCTYPE html><html>");
						client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
						client.println("<link rel=\"icon\" href=\"data:,\">");
						// Estilo CSS para o botão
						client.println("<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}");
						client.println(".button { background-color: #4CAF50; border: none; color: white; padding: 16px 40px;");
						client.println("text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}");
						client.println(".button2 {background-color: #555555;}</style></head>");

						// Título da página
						client.println("<body><h1>ESP32 Web Server</h1>");

						// Mostra o estado atual do LED na página web
						client.println("<p>LED - State " + LEDestado + "</p>");
						// Caso o LED esteja desligado, mostrar o botão para
						// ligar e vice-versa
						if (LEDestado=="off") {
							client.println("<p><a href=\"/led/on\"><button class=\"button\">ON</button></a></p>");
						} else {
							client.println("<p><a href=\"/led/off\"><button class=\"button button2\">OFF</button></a></p>");
						}

						client.println("</body></html>");

						// Resposta HTTP termina por uma linha em branco
						client.println();
						// Quebrando o while
						break;
					} else {
						// Caso obteve uma nova linha, então esvazie a variável
						// de linha atual
						currentLine = "";
					}
				} else if (c != '\r') { // Caso obteve uma informação diferente que nova linha
					// Adicione à variável
					currentLine += c;
				}
			}
		}
		// Limpar a variável do cabeçalho
		header = "";
		// Fechar conexão
		client.stop();
		Serial.println("Cliente desconectado");
		Serial.println("");
	}
}
